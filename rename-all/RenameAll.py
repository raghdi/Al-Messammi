#! /usr/bin/python3
# -*- coding: utf-8 -*-

##############################################################################
#a########  "قُلۡ بِفَضلِ ٱللَّهِ وَبِرَحمَتِهِۦ فَبِذَٰلِكَ فَليَفرَحُواْ هُوَ خَيرُُ مِّمَّا يَجمَعُونَ"  ########
##############################################################################

#a============================================================================
#a                                 المشروع : برنامج تسمية عدة ملفات دفعة واحدة  
#a                                                             الإصدارة : 0.3.0 
#a                                <asmaaarab@gmail.com>    المطور  : رغدي أحمد 
#a  http://www.ojuba.org/wiki/doku.php/waqf/license  الرخصة  : رخصة وقف العامة
#a============================================================================

import gi
gi.require_version ("Gtk", "4.0")
from gi.repository import Gtk, Gio, GObject, Pango
from os.path import join, dirname, realpath, exists, splitext, basename, isdir
from os import rename
import re

Gtk.Widget.set_default_direction (Gtk.TextDirection.RTL)


SOWAR_ALQURAN_ARABIC = ["الفاتحة", "البقرة", "آل عمران", "النساء", "المائدة", "الأنعام", "الأعراف", 
    "الأنفال", "التوبة", "يونس", "هود", "يوسف", "الرعد", "إبراهيم", "الحجر", "النحل", "الإسراء", "الكهف", "مريم", "طه", 
    "الأنبياء", "الحج", "المؤمنون", "النور", "الفرقان", "الشعراء", "النمل", "القصص", "العنكبوت", "الروم", "لقمان", 
    "السجدة", "الأحزاب", "سبأ", "فاطر", "يس", "الصافات", "ص", "الزمر", "غافر", "فصلت", "الشورى", "الزخرف", 
    "الدخان", "الجاثية", "الأحقاف", "محمد", "الفتح", "الحجرات", "ق", "الذاريات", "الطور", "النجم", "القمر", "الرحمن", 
    "الواقعة", "الحديد", "المجادلة", "الحشر", "الممتحنة", "الصف", "الجمعة", "المنافقون", "التغابن", "الطلاق", 
    "التحريم", "الملك", "القلم", "الحاقة", "المعارج", "نوح", "الجن", "المزمل", "المدثر", "القيامة", "الإنسان", "المرسلات", 
    "النبأ", "النازعات", "عبس", "التكوير", "الانفطار", "المطففين", "الانشقاق", "البروج", "الطارق", "الأعلى", "الغاشية", 
    "الفجر", "البلد", "الشمس", "الليل", "الضحى", "الشرح", "التين", "العلق", "القدر", "البينة", "الزلزلة", "العاديات", 
    "القارعة", "التكاثر", "العصر", "الهمزة", "الفيل", "قريش", "الماعون", "الكوثر", "الكافرون", "النصر", "المسد", 
    "الإخلاص", "الفلق", "الناس" ]

SOWAR_ALQURAN_LATIN = ["AlFatihah", "AlBaqarah ", "Al'Imran", "AnNisa'", "AlMa'idah", "AlAn'am",
    "AlA'raf", "AlAnfal", "AtTaubah", "Yunus", "Hud", "Yusuf", "ArRa'd", "Ibrahim", "AlHijr", "AnNahl", 
    "AlIsra'", "AlKahf", "Maryam", "TaHa", "AlAnbiya'", "AlHajj", "AlMu'minun", "AnNur", "AlFurqan", 
    "AshShu'ara'", "AnNaml", "AlQasas", "AlAnkabut", "ArRum", "Luqman", "AsSajdah", "AlAhzab", 
    "Saba'", "Fatir", "YaSin", "AsSaffat", "Sad", "AzZumar", "Ghafir", "Fussilat", "AshShura", "AzZukhruf", 
    "AdDukhan", "AlJathiyah", "AlAhqaf", "Muhammad", "AlFath", "AlHujurat", "Qaf", "AdhDhariyat", 
    "AtTur", "AnNajm", "Qamar", "ArRahman", "AlWaqi'ah", "AlHadid", "AlMujadilah", "AlHashr", 
    "AlMumtahanah", "AsSaff", "AlJumu'ah", "AlMunafiqun", "AtTaghabun", "AtTalaq", "AtTahrim", 
    "AlMulk", "AlQalam", "AlHaqqah", "AlMa'arij", "Nuh", "AlJinn", "AlMuzzammil", "AlMuddaththir", 
    "AlQiyamah", "AlInsan", "AlMursalat", "AnNaba'", "AnNazi'at", "Abasa", "AtTakwir", "AlInfitar", 
    "AlMutaffifin", "AlInshiqaq", "AlBuruj", "AtTariq", "AlA'la", "AlGhashiyah", "AlFajr", "AlBalad", 
    "AshShams", "AlLail", "AdDuha", "AshSharh", "AtTin", "AlAlaq", "AlQadr", "AlBayyinah", "AzZalzalah", 
    "AlAadiyat", "AlQari'ah", "AtTakathur", "AlAsr", "AlHumazah", "AlFil", "Quraish", "AlMa'un", 
    "AlKauthar", "AlKafirun", "AnNasr", "AlMasad", "AlIkhlas", "AlFalaq", "AnNas"]


LETTERS_HIDJAI = ["ا", "ب", "ت", "ث", "ج", "ح", "خ", "د", "ذ", "ر", "ز", "س", "ش", "ص",
                   "ض", "ط", "ظ", "ع", "غ", "ف", "ق", "ك", "ل", "م", "ن", "ه", "و", "ي" ]

LETTERS_ABJADI = ["ا", "ب", "ج", "د", "ه", "و", "ز", "ح", "ط", "ي", "ك", "ل", "م", "ن",
                   "س", "ع", "ف", "ص", "ق", "ر", "ش", "ت", "ث", "خ", "ذ", "ض", "ظ", "غ" ]

LETTERS_LATIN = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
                  "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

NOMBER_N = {"0":"٠", "1":"١", "2":"٢", "3":"٣", "4":"٤", "5":"٥‌", "6":"٦", "7":"٧", "8":"٨", "9":"٩"}

#NOMBER_N = {"٠":"0", "١":"1", "٢":"2", "٣":"3", "٤":"4", "٥‌":"5", "٦":"6", "٧":"7", "٨":"8", "٩":"9"}

NAME_ORDINAL_ONE = {0:"o",1:"الحادي",2:"الثاني",3:"الثالث",4:"الرابع",5:"الخامس",6
                    :"السادس",7:"السابع",8:"الثامن",9:"التاسع"}

NAME_ORDINAL_TEN = {0:"o",1:"عشر",2:"العشرون",3:"الثلاثون",4:"الأربعون",5:"الخمسون",
                    6:"الستون",7:"السبعون",8:"الثمانون",9:"التسعون"}

NAME_ORDINAL_HUNDRED = {0:"o",1:"المائة",2:"المائتان",3:"الثلاثمائة",4:"الأربعمائة",5:"الخمسمائة",
                        6:"الستمائة",7:"السبعمائة",8:"الثمانمائة",9:"التسعمائة"}

MY_RE = {
    "الحروف العربية":"[أ-يءًٌٍَُِّْ]",
    "الحروف اللاتينة الصغيرة":"[a-z]", 
    "الحروف اللاتينة الكبيرة":"[A-Z]", 
    "كل الحروف اللاتينة":"[a-zA-Z]", 
    "الأرقام":"\d", 
    "الرموز":"[^\w\s]", 
    "المسافات":"\s", 
    "كل شيء ما عدا الحروف العربية":"[^أ-يءًٌٍَُِّْ\s]", 
    "كل شيء ما عدا الحروف اللاتينة":"[^a-zA-Z\s]", 
    "كل شيء ما عدا الأرقام":"[^\d]", 
    "كل شيء ما عدا الرموز":"[\w\s]", 
    "كل شيء":".*" 
    }


#a------------------------------------------
def info (parent, msg):
    dialog = Gtk.MessageDialog (
    transient_for=parent,
    message_type=Gtk.MessageType.INFO,
    buttons=Gtk.ButtonsType.OK,
    text=msg,
    )
    dialog.connect ("response", lambda *a: dialog.destroy ())
    dialog.show ()

#a------------------------------------------
def get_ordinal (n, i):
    a3 = int (n/100)
    a2 = int ( (n%100)/10)
    a1 = int (n%10)
    name = "{} و{} بعد {}".format (NAME_ORDINAL_ONE[a1],NAME_ORDINAL_TEN[a2],NAME_ORDINAL_HUNDRED[a3])
    name = name.replace (" وo", "")
    name = name.replace (" بعد o", "")
    name = name.replace ("o وعشر", "العاشر")
    name = name.replace ("o و", "")
    name = name.replace ("o بعد", "")
    name = name.replace ("وعشر", "عشر")
    if a2 == 0: name = name.replace ("الحادي", "الأول")
    if i == 1:
        name = name.replace (r"عشر", r"عشرة")
        name = name.replace (r"العاشر", r"العاشرة")
        name = name.replace (r"الحادي", r"الحادية")
        name = name.replace (r"الثاني", r"الثانية")
        name = name.replace (r"الثالث", r"الثالثة")
        name = name.replace (r"الرابع", r"الرابعة")
        name = name.replace (r"الخامس", r"الخامسة")
        name = name.replace (r"السادس", r"السادسة")
        name = name.replace (r"السابع", r"السابعة")
        name = name.replace (r"الثامن", r"الثامنة")
        name = name.replace (r"التاسع", r"التاسعة")
        name = name.replace (r"الأول", r"الأولى")
        name = name.replace (r"عشرةو", r"عشرو")
    return name

#a------------------------------------------
class RenameAll (Gtk.Application): 
    
    def __init__ (self, *a):
        super ().__init__ (application_id="org.ahmedraghdi.renameall", flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.connect ("activate", self.on_activate)

    def on_activate (self, *a):
        self.win = Gtk.ApplicationWindow (application=self, title="المسمي")
        self.win.set_icon_name ("document-edit-symbolic")
        self.now_page = 0
        self.build ()
        self.win.present ()
        self.win.set_default_size (900, 700)
    
    #a------------------------------------------
    def sure (self, msg, sel_tree, my_fanc, all=0):
        if len (self.store_files) == 0 and all==2: return #rename_all
        if len (self.store_files) == 0 and all==3: return #remove_all_files
        if len (self.store_option) == 0 and all==4: return #remove_all_options
        model, i = sel_tree.get_selected ()
        if not i and all==0: return
        dialog = Gtk.MessageDialog (
        transient_for=self.win,
        message_type=Gtk.MessageType.WARNING,
        buttons=Gtk.ButtonsType.YES_NO,
        text=msg,
        )
        dialog.connect ("response", my_fanc)
        dialog.show ()
    
    def process_existed_name (self, text, path_file):
        new_file = join (dirname (path_file), text)
        if exists (new_file):
            for b in range (10000):
                filename, extension = self.split_ext (path_file, text)
                text1 = filename+"_"+str (b)+extension
                if not exists (join (dirname (path_file), text1)):
                    text = text1
                    break
        return text
    
    def start_rename_files (self, dialog, res):
        dialog.destroy ()
        if res == Gtk.ResponseType.YES:
            s = len (self.store_files)-1
            while s >= 0:
                iter0 = self.store_files.get_iter (s)
                new_name = self.store_files.get_value (iter0, 2)
                old_file = self.store_files.get_value (iter0, 3)
                if self.store_files.get_value (iter0, 0):
                    new_name = self.process_existed_name (new_name, old_file)
                    new_file = join (dirname (old_file), new_name)
                    rename (old_file, new_file)
                    self.store_files.remove (iter0)
                s -= 1
            info (self.win, "تم تسمية جميع الملفات المراد إعادة تسميتها")
           
    
    def split_ext (self, file_path, file_name):
        if not isdir (file_path): filename, extension = splitext (file_name)
        else: filename, extension = file_name, ""
        return filename, extension  
    
    def process_removing (self, text, option_ls):
        #===========================================
        # option_ls = [option, naw3, re or text, type]
        if option_ls[1] == 0:
            if option_ls[3] == 0:
                text = text.replace (option_ls[2], "")
            if option_ls[3] == 1:
                for t in option_ls[2].split (" "):
                    text = text.replace (t, "")
            if option_ls[3] == 2:
                for l in list (option_ls[2]):
                    text = text.replace (l, "")
            else:
                text = re.sub (option_ls[2], "", text)
        else:
            text = re.sub (option_ls[2], "", text)
        return text
    
    def process_replacing (self, text, option_ls):
        #===========================================
        # option_ls = [option, naw3, re or text, new-text]
        if option_ls[1] == 0:
            text = text.replace (option_ls[2], option_ls[3])
        else:
            text = re.sub (option_ls[2], option_ls[3], text)
        return text
    
    def process_adding (self, text, option_ls, s):
        #===========================================
        # option_ls = [option, naw3, first or text, reverse, pos, type]
        v = option_ls[4]
        #------------------------------------------------------
        if option_ls[1] == 0:
            if option_ls[4] == 0: text = option_ls[2]+text
            elif option_ls[4] == -1: text = text+option_ls[2]
            else: text = text[:v]+option_ls[2]+text[v:]
        #------------------------------------------------------     
        if option_ls[1] == 1:
            try:
                i = int (option_ls[2])
                if option_ls[3] == 1: n = i-s
                else: n = s+i
            except:
                if option_ls[3] == 1: n = len (self.store_files)-s
                else: n = s+1
            if option_ls[5] == 1: 
                sn = str (n).replace ("0","٠").replace ("1","١").replace ("2",
                                    "٢").replace ("3","٣").replace ("4","٤").replace ("5",
                                    "٥‌").replace ("6","٦").replace ("7",
                                    "٧").replace ("8","٨").replace ("9","٩")
            else: sn = str (n)
            if option_ls[4] == 0: text = sn+text
            elif option_ls[4] == -1: text = text+sn
            else: text = text[:v]+sn+text[v:]

        #------------------------------------------------------     
        if option_ls[1] == 2:
            try: 
                if option_ls[5] == 0: i = LETTERS_HIDJAI.index (option_ls[2].strip (), ) 
                else: i = LETTERS_ABJADI.index (option_ls[2] .strip (), ) 
                if option_ls[3] == 1: n_la = i-s
                else: n_la = s+i
            except: 
                if option_ls[3] == 1: n_la = 27-s
                else: n_la = s+0
            if n_la < 0: n_la = (n_la%27)+28
            if n_la > 27: n_la = (n_la%27)-1
            if option_ls[5] == 0: letter = LETTERS_HIDJAI[n_la]
            else:  letter = LETTERS_ABJADI[n_la]
            if option_ls[4] == 0: text = letter+text
            elif option_ls[4] == -1: text = text+letter
            else: text = text[:v]+letter+text[v:]
            
        #------------------------------------------------------     
        if option_ls[1] == 3:
            try: 
                i = LETTERS_LATIN.index (option_ls[2].strip ().lower (), ) 
                if option_ls[3] == 1: n_la = i-s
                else: n_la = s+i
            except: 
                if option_ls[3] == 1: n_la = 25-s
                else: n_la = s+0
            if n_la < 0: n_la = (n_la%25)+26
            if n_la > 25: n_la = (n_la%25)-1
            if option_ls[5] == 0: letter = LETTERS_LATIN[n_la]
            else: letter = LETTERS_LATIN[n_la].upper ()
            if option_ls[4] == 0: text = letter+text
            elif option_ls[4] == -1: text = text+letter
            else: text = text[:v]+letter+text[v:]
            
        #------------------------------------------------------     
        if option_ls[1] == 4:
            try:
                i = int (option_ls[2])
                if option_ls[3] == 1: n = i-s
                else: n = s+i
            except:
                if option_ls[3] == 1: n = len (self.store_files)-s
                else: n = s+1
            if option_ls[5] == 1: sno = get_ordinal (n, 1)
            else: sno = get_ordinal (n, 0)
            if option_ls[4] == 0: text = sno+text
            elif option_ls[4] == -1: text = text+sno
            else: text = text[:v]+sno+text[v:]
            
        #------------------------------------------------------     
        if option_ls[1] == 5:
            try: 
                i = SOWAR_ALQURAN_ARABIC.index (option_ls[2].strip (), ) 
                if option_ls[3] == 1: n_sr = i-s
                else: n_sr = s+i
            except: 
                if option_ls[3] == 1: n_sr = 113-s
                else: n_sr = s+0
            if n_sr < 0: n_sr = (n_sr%113)+114
            if n_sr > 113: n_sr = (n_sr%113)-1
            if option_ls[5] == 0: sora = SOWAR_ALQURAN_ARABIC[n_sr]
            else: sora = SOWAR_ALQURAN_LATIN[n_sr]
            if option_ls[4] == 0: text = sora+text
            elif option_ls[4] == -1: text = text+sora
            else: text = text[:v]+sora+text[v:]
        
        #------------------------------------------------------  
        return text
    
    def process_rename (self, text, s):
        if self.set_new_name.get_active ():
            text = self.entry_new_name.get_text ()
        for b in range (len (self.store_option)):
            iter1 = self.store_option.get_iter (b)
            option_ls = eval (self.store_option.get_value (iter1, 0))
            if option_ls[0] == 1: text = self.process_adding (text, option_ls, s)
            elif option_ls[0] == 2: text = self.process_replacing (text, option_ls)
            elif option_ls[0] == 3: text = self.process_removing (text, option_ls)
        return text
    
    def rename_files_in_treeview (self, *a):
        s = 0
        for a in range (len (self.store_files)):
            iter0 = self.store_files.get_iter (a)
            if self.store_files.get_value (iter0, 0):
                filename, extension = self.split_ext (self.store_files.get_value (iter0, 3),
                                                      self.store_files.get_value (iter0, 1))
                #===========================================
                if self.process_combo.get_active () == 0:
                    new_text = self.process_rename (filename, s)
                    new_name = new_text+extension
                elif self.process_combo.get_active () == 1:
                    new_text = "."+self.process_rename (extension, s)
                    new_text = new_text.replace ("..", ".")
                    new_name = filename+new_text
                elif self.process_combo.get_active () == 2:
                    new_name = self.process_rename (filename+extension, s)
                #===========================================
                self.store_files.set_value (iter0, 2, new_name)
                s += 1
    
    def move_file_cb (self, sel_tree, v):
        model, i = sel_tree.get_selected ()
        if i:
            if v == 1:
                i0 = model.iter_next (i)
                model.move_after (i, i0)
            if v == -1:
                i0 = model.iter_previous (i)
                model.move_before (i, i0)
            self.rename_files_in_treeview ()
    
    def remove_file_cb (self, dialog, res):
        model, i = self.sel_files.get_selected ()
        if res == Gtk.ResponseType.YES:
            model.remove (i)
            self.rename_files_in_treeview ()
        dialog.destroy ()
    
    def remove_all_files_cb (self, dialog, res):
        if res == Gtk.ResponseType.YES:
            self.store_files.clear ()
            self.rename_files_in_treeview ()
        dialog.destroy ()
        
    def remove_option_cb (self, dialog, res):
        model, i = self.sel_option.get_selected ()
        if res == Gtk.ResponseType.YES:
            model.remove (i)
            self.rename_files_in_treeview ()
        dialog.destroy ()
    
    def remove_all_options_cb (self, dialog, res):
        if res == Gtk.ResponseType.YES:
            self.store_option.clear ()
            self.rename_files_in_treeview ()
        dialog.destroy ()
    
    def add_files_cb (self, *args):
        add_dlg = Gtk.FileChooserDialog (title="اختر ملفات للتسمية", transient_for=self.win, 
                                        action=Gtk.FileChooserAction.OPEN)
        add_dlg.add_buttons (
            "ألغ", Gtk.ResponseType.CANCEL,
            "افتح", Gtk.ResponseType.OK,
        )
        add_dlg.set_select_multiple (True)
        def on_response (w, r):
            if r == Gtk.ResponseType.OK:
                for i in add_dlg.get_files ():
                    p = i.get_path ()
                    m = self.store_files.append ()
                    self.store_files.set (m, 0, True, 1, basename (p), 2, basename (p), 3, p)
            add_dlg.destroy ()
            self.rename_files_in_treeview ()
        add_dlg.connect ("response", on_response)
        add_dlg.show ()
        
    def add_folders_cb (self, *args):
        add_dlg = Gtk.FileChooserDialog (title="اختر مجلدات للتسمية", transient_for=self.win, 
                                      action=Gtk.FileChooserAction.SELECT_FOLDER)
        add_dlg.add_buttons (
            "ألغ", Gtk.ResponseType.CANCEL,
            "أضف", Gtk.ResponseType.OK,
        )
        add_dlg.set_select_multiple (True)
        def on_response (w, r):
            if r == Gtk.ResponseType.OK:
                for i in add_dlg.get_files ():
                    p = i.get_path ()
                    m = self.store_files.append ()
                    self.store_files.set (m, 0, True, 1, basename (p), 2, basename (p), 3, p)
            add_dlg.destroy ()
            self.rename_files_in_treeview ()
        add_dlg.connect ("response", on_response)
        add_dlg.show ()
    
    def fixed_toggled_field (self, cell, path, model):
        itr = model.get_iter ( (path),)
        fixed = model.get_value (itr, 0)
        fixed = not fixed
        model.set (itr, 0, fixed)
    
    def set_option_remove (self, btn):
        ls_option = [] #  = [option, naw3, re or text, type]
        #option: delete=3
        #naw3: text=0, re=1
        #type: text=0, words=1, letters=2, re=3
        ls_del = [r"حذف نص",
              r"حذف نوع"]
        #option---------------------------------
        text1 = btn.get_label ()
        text = btn.get_label ()
        ls_option.append (3)
        #naw3---------------------------------
        ls_option.append (ls_del.index (text, ))
        #text---------------------------------
        if ls_del.index (text, ) == 0:
            text += " '{}' ".format (self.entry_del_text.get_text (),)
            ls_option.append (self.entry_del_text.get_text (), )
            text += " على شكل: "+self.del_text_option.get_active_text ()
            ls_option.append (self.del_text_option.get_active (), )
        else:
            text += " عبارة عن: "+self.del_naw3_option.get_active_text ()
            ls_option.append (MY_RE[self.del_naw3_option.get_active_text ()], )
        #set in liststore---------------------------------
        text_option = repr (ls_option)
        m = self.store_option.append () 
        self.store_option.set (m, 0, text_option, 1, text)
        #change---------------------------------
        self.rename_files_in_treeview ()
    
    def set_option_replace (self, btn):
        ls_option = [] #  = [option, naw3, re or text, new-text]
        #option: replace=2
        #naw3: text=0, re=1
        ls_rp = [r"استبدال نص",
              r"استبدال نوع"]
        #option---------------------------------
        text = btn.get_label ()
        ls_option.append (2)
        #naw3---------------------------------
        if self.re_replace_text.get_active (): ls_option.append (1)
        else: ls_option.append (ls_rp.index (text, ))
        #text---------------------------------
        if ls_rp.index (text, ) == 0:
            text += " هو '{}' ".format (self.entry_replace_text_old.get_text (),)
            ls_option.append (self.entry_replace_text_old.get_text (), )
            if self.re_replace_text.get_active (): text += " (على شكل تعبير قياسي) "
            text += " بالنص الجديد '{}' ".format (self.entry_replace_text_new.get_text (),)
            ls_option.append (self.entry_replace_text_new.get_text (), )
        else:
            text += " عبارة عن: "+self.replace_naw3_option.get_active_text ()
            ls_option.append (MY_RE[self.replace_naw3_option.get_active_text ()], )
            text += " بالنص الجديد '{}' ".format (self.entry_replace_naw3_new.get_text (),)
            ls_option.append (self.entry_replace_naw3_new.get_text (), )
        #set in liststore---------------------------------
        text_option = repr (ls_option)
        m = self.store_option.append () 
        self.store_option.set (m, 0, text_option, 1, text)
        #change---------------------------------
        self.rename_files_in_treeview ()
    
    def set_option_add (self, btn, my_object):
        ls_option = [] #  = [option, naw3, first or text, reverse, pos, type]
        #option: add=1, replace=2, delete=3
        #naw3: text=0, numbers=1, letters_ar=2, letters_ar=3, ordinal=4, suar=5
        #pos: after=0, before=-1, between=1,2,3...
        #reverse: normal=0, reverse=1
        ls_add = [r"أضف نصًا",
              r"أضف ترقيمًا مرتبًا",
              r"أضف حروفًا عربية",
              r"أضف أحرفًا لاتينية",
              r"أضف أعدادا ترتيبية",
              r"أضف أسماء السور"]
        #option---------------------------------
        text1 = btn.get_label ()
        text = btn.get_label ()
        ls_option.append (1)
        #naw3---------------------------------
        ls_option.append (ls_add.index (text, ))
        #first---------------------------------
        ls_option.append (my_object.entry_start.get_text ())
        if my_object.entry_start.get_text () != "": 
            text += " ، البداية من '"+my_object.entry_start.get_text ()+"'"
        #reverse---------------------------------
        if my_object.reverse.get_active ():
            text += " عكسيًّا" 
            ls_option.append (1)
        else: ls_option.append (0)
        #pos---------------------------------
        text += " ، الموضع "+my_object.pos_object.get_active_text ()
        if my_object.pos_object.get_active () == 0:
            ls_option.append (0)
        elif my_object.pos_object.get_active () == 1: 
            text += " بعد الحرف "+str (int (my_object.pos_after.get_value ()))
            ls_option.append (int (my_object.pos_after.get_value ()))
        elif my_object.pos_object.get_active () == 2:
            ls_option.append (-1)
        #type---------------------------------
        if ls_add.index (text1, ) > 0: 
            text += " ، "+my_object.type_object.get_active_text ()
            ls_option.append (my_object.type_object.get_active ())
        else:
            text = text.replace (r"نصًا ، البداية من", r"نصًا هو ")
        #set in liststore---------------------------------
        text_option = repr (ls_option)
        m = self.store_option.append () 
        self.store_option.set (m, 0, text_option, 1, text)
        #change---------------------------------
        self.rename_files_in_treeview ()
    
    def build (self, *a):
        hb_bar = Gtk.HeaderBar ()
        self.win.set_titlebar (hb_bar)
        
        #----------------------------------------------------------------------------------
        self.store_files = Gtk.ListStore (GObject.TYPE_BOOLEAN, GObject.TYPE_STRING, 
                                          GObject.TYPE_STRING, GObject.TYPE_STRING)
        btnbox1 = Gtk.Box (orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class (btnbox1.get_style_context (), "linked")
        
        add_files = Gtk.Button.new_from_icon_name ("document-new-symbolic")
        add_files.set_tooltip_text ("أضف ملفات")
        add_files.connect ("clicked", self.add_files_cb)
        btnbox1.append (add_files)
        
        add_folder = Gtk.Button.new_from_icon_name ("folder-new-symbolic")
        add_folder.set_tooltip_text ("أضف مجلدات")
        add_folder.connect ("clicked", self.add_folders_cb)
        btnbox1.append (add_folder)
        
        go_up = Gtk.Button.new_from_icon_name ("go-up-symbolic")
        go_up.set_tooltip_text ("ارفع الملف المحدد")
        go_up.connect ("clicked", lambda *a: self.move_file_cb (self.sel_files, -1))
        btnbox1.append (go_up)
        
        go_down = Gtk.Button.new_from_icon_name ("go-down-symbolic")
        go_down.set_tooltip_text ("أنزل الملف المحدد")
        go_down.connect ("clicked", lambda *a: self.move_file_cb (self.sel_files, 1))
        btnbox1.append (go_down)
        
        remove = Gtk.Button.new_from_icon_name ("edit-delete-symbolic")
        remove.set_tooltip_text ("حذف الملف المحدد")
        remove.connect ("clicked", lambda *a: self.sure ("هل تريد حذف الملف المحدد؟", 
                                            self.sel_files, self.remove_file_cb, 0))
        btnbox1.append (remove)
        
        remove_all = Gtk.Button.new_from_icon_name ("edit-clear-all-symbolic")
        remove_all.set_tooltip_text ("حذف جميع الملفات")
        remove_all.connect ("clicked",lambda *a: self.sure ("هل تريد حذف جميع الملفات؟", 
                                            self.sel_files, self.remove_all_files_cb, 3))
        btnbox1.append (remove_all)
        
        rename_btn = Gtk.Button.new_with_label ("أعد التسمية")
        Gtk.StyleContext.add_class (rename_btn.get_style_context (), "suggested-action")
        rename_btn.set_tooltip_text ("أعد تسمية الملفات")
        rename_btn.connect ("clicked", lambda *a: self.sure ("هل تريد إعادة تسمية الملفات؟", 
                                            self.sel_files, self.start_rename_files, 2))
        hb_bar.pack_start (btnbox1)
        hb_bar.set_title_widget (rename_btn)
        
        self.process_combo = Gtk.ComboBoxText ()
        self.process_combo.set_entry_text_column (0)
        self.process_combo.append_text ("معالجة الأسماء")
        self.process_combo.append_text ("معالجة الامتداد")
        self.process_combo.append_text ("معالجة الكل")
        self.process_combo.connect ("changed", self.rename_files_in_treeview)
        self.process_combo.set_active (0)
        hb_bar.pack_end (self.process_combo)
        
        #------------------------------------------------------------------------------------
        vbox_main = Gtk.Box (spacing=3,orientation=Gtk.Orientation.VERTICAL)
        vbox_main.set_margin_start (3)
        vbox_main.set_margin_end (3)
        vbox_main.set_margin_bottom (3)
        vbox_main.set_margin_top (3)
        
        self.tree_files = Gtk.TreeView.new_with_model (self.store_files)
        self.sel_files = self.tree_files.get_selection ()
        celltext = Gtk.CellRendererText ()
        celltext.set_property ("ellipsize", Pango.EllipsizeMode.END)
        celltoggle = Gtk.CellRendererToggle ()
        celltoggle.set_property ("activatable", True)
        columntoggle = Gtk.TreeViewColumn ("#", celltoggle)
        columntoggle.add_attribute ( celltoggle, "active", 0)
        celltoggle.connect ("toggled", self.fixed_toggled_field, self.store_files)
        self.tree_files.append_column (columntoggle)
        columntext_now= Gtk.TreeViewColumn ("الاسم الحالي", celltext, text = 1 )
        columntext_now.set_expand (True)
        self.tree_files.append_column (columntext_now)
        columntext_new = Gtk.TreeViewColumn ("الاسم الجديد", celltext, text = 2 )
        columntext_new.set_expand (True)
        self.tree_files.append_column (columntext_new)
        scroll = Gtk.ScrolledWindow ()
        scroll.set_child (self.tree_files)
        scroll.set_vexpand (True)
        vbox_main.append (scroll)
        
        #-------------------------------------------------------------------------------------------
        hbox = Gtk.Box (spacing=3,orientation=Gtk.Orientation.HORIZONTAL)
        self.set_new_name = Gtk.CheckButton.new_with_label ("اسم جديد ")
        self.set_new_name.connect ("toggled", self.rename_files_in_treeview)
        hbox.append (self.set_new_name)
        self.entry_new_name = Gtk.Entry ()
        self.entry_new_name.set_hexpand (True)
        self.entry_new_name.connect ("changed", self.rename_files_in_treeview)
        self.entry_new_name.set_placeholder_text ("ضع اسمًا جديدًا")
        hbox.append (self.entry_new_name)
        vbox_main.append (hbox)
        
        #--------------------------------------------------------------------------------------------
        notebk = Gtk.Notebook ()
        notebk.set_tab_pos (Gtk.PositionType.RIGHT)
        grid = Gtk.Grid ()
        grid.set_margin_bottom (5)
        grid.set_margin_start (5)
        grid.set_margin_top (5)
        grid.set_margin_end (5)
        grid.set_row_spacing (5)
        grid.set_column_spacing (5)
        
        self.btn_set_text = custom_object (r"أضف نصًا",
                            r"النص المضاف", [r"مغربي", r"مشرقي"], self.rename_files_in_treeview)
        self.btn_set_text.add.connect ("clicked", self.set_option_add, self.btn_set_text)
        grid.attach (self.btn_set_text.add, 0, 0, 2, 1)
        grid.attach (self.btn_set_text.entry_start, 2, 0, 3, 1)
        grid.attach (self.btn_set_text.pos_object, 5, 0, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 0, 1, 1)
        grid.attach (self.btn_set_text.pos_after, 7, 0, 1, 1)
        
        self.btn_set_nomber = custom_object (r"أضف ترقيمًا مرتبًا",
                            r"أول رقم", [r"مغربي", r"مشرقي"], self.rename_files_in_treeview)
        self.btn_set_nomber.add.connect ("clicked", self.set_option_add, self.btn_set_nomber)
        grid.attach (self.btn_set_nomber.add, 0, 1, 2, 1)
        grid.attach (self.btn_set_nomber.entry_start, 2, 1, 2, 1)
        grid.attach (self.btn_set_nomber.reverse, 4, 1, 1, 1)
        grid.attach (self.btn_set_nomber.pos_object, 5, 1, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 1, 1, 1)
        grid.attach (self.btn_set_nomber.pos_after, 7, 1, 1, 1)
        grid.attach (self.btn_set_nomber.type_object, 8, 1, 1, 1)
        
        self.btn_set_sora = custom_object (r"أضف أسماء السور",
                            r"أول سورة (عربي)", [r"عربية", r"انكليزية"], self.rename_files_in_treeview)
        self.btn_set_sora.add.connect ("clicked", self.set_option_add, self.btn_set_sora)
        grid.attach (self.btn_set_sora.add, 0, 5, 2, 1)
        grid.attach (self.btn_set_sora.entry_start, 2, 5, 2, 1)
        grid.attach (self.btn_set_sora.reverse, 4, 5, 1, 1)
        grid.attach (self.btn_set_sora.pos_object, 5, 5, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 5, 1, 1)
        grid.attach (self.btn_set_sora.pos_after, 7, 5, 1, 1)
        grid.attach (self.btn_set_sora.type_object, 8, 5, 1, 1)

        self.btn_set_letter_arabic = custom_object (r"أضف حروفًا عربية",
                                    r"أول حرف", [r"هجائية", r"أبجدية"], self.rename_files_in_treeview)
        self.btn_set_letter_arabic.add.connect ("clicked", self.set_option_add, self.btn_set_letter_arabic)
        grid.attach (self.btn_set_letter_arabic.add, 0, 2, 2, 1)
        grid.attach (self.btn_set_letter_arabic.entry_start, 2, 2, 2, 1)
        grid.attach (self.btn_set_letter_arabic.reverse, 4, 2, 1, 1)
        grid.attach (self.btn_set_letter_arabic.pos_object, 5, 2, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 2, 1, 1)
        grid.attach (self.btn_set_letter_arabic.pos_after, 7, 2, 1, 1)
        grid.attach (self.btn_set_letter_arabic.type_object, 8, 2, 1, 1)
        
        self.btn_set_letter_latin = custom_object (r"أضف أحرفًا لاتينية",
                                    r"أول حرف", [r"صغيرة", r"كبيرة"], self.rename_files_in_treeview)
        self.btn_set_letter_latin.add.connect ("clicked", self.set_option_add, self.btn_set_letter_latin)
        grid.attach (self.btn_set_letter_latin.add, 0, 3, 2, 1)
        grid.attach (self.btn_set_letter_latin.entry_start, 2, 3, 2, 1)
        grid.attach (self.btn_set_letter_latin.reverse, 4, 3, 1, 1)
        grid.attach (self.btn_set_letter_latin.pos_object, 5, 3, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 3, 1, 1)
        grid.attach (self.btn_set_letter_latin.pos_after, 7, 3, 1, 1)
        grid.attach (self.btn_set_letter_latin.type_object, 8, 3, 1, 1)
        
        self.btn_set_ordinal = custom_object (r"أضف أعدادا ترتيبية",
                                r"أول مرتبة (رقم)", [r"مذكرة", r"مؤنثة"], self.rename_files_in_treeview)
        self.btn_set_ordinal.add.connect ("clicked", self.set_option_add, self.btn_set_ordinal)
        grid.attach (self.btn_set_ordinal.add, 0, 4, 2, 1)
        grid.attach (self.btn_set_ordinal.entry_start, 2, 4, 2, 1)
        grid.attach (self.btn_set_ordinal.reverse, 4, 4, 1, 1)
        grid.attach (self.btn_set_ordinal.pos_object, 5, 4, 1, 1)
        grid.attach (Gtk.Label.new ("بعد الحرف :"), 6, 4, 1, 1)
        grid.attach (self.btn_set_ordinal.pos_after, 7, 4, 1, 1)
        grid.attach (self.btn_set_ordinal.type_object, 8, 4, 1, 1)
        
        notebk.append_page (grid, Gtk.Label.new ("إضافة"))
        
        #--------------------------------------------------------------------------------------------
        grid = Gtk.Grid ()
        grid.set_margin_bottom (5)
        grid.set_margin_start (5)
        grid.set_margin_top (5)
        grid.set_margin_end (5)
        grid.set_row_spacing (5)
        grid.set_column_spacing (5)
        
        self.del_text = Gtk.Button.new_with_label ("حذف نص")
        self.del_text.connect ("clicked", self.set_option_remove)
        self.entry_del_text = Gtk.Entry ()
        self.del_text_option= Gtk.ComboBoxText ()
        for a in ["نص",
                  "كلمات",
                  "حروف",
                  "تعبير قياسي" ]:
            self.del_text_option.append_text (a)
        self.del_text_option.set_active (0)
        grid.attach (self.del_text, 0, 0, 1, 1)
        grid.attach (self.entry_del_text, 1, 0, 4, 1)
        grid.attach (self.del_text_option, 5, 0, 1, 1)
        
        self.del_naw3 = Gtk.Button.new_with_label ("حذف نوع")
        self.del_naw3.connect ("clicked", self.set_option_remove)
        self.del_naw3_option= Gtk.ComboBoxText ()
        for a in ["الحروف العربية", "الحروف اللاتينة الصغيرة",
                   "الحروف اللاتينة الكبيرة",
                    "كل الحروف اللاتينة", "الأرقام", "الرموز", "المسافات"
                  ,"كل شيء ما عدا الحروف العربية", "كل شيء ما عدا الحروف اللاتينة",
                   "كل شيء ما عدا الأرقام", "كل شيء ما عدا الرموز", "كل شيء"]:
            self.del_naw3_option.append_text (a)
        self.del_naw3_option.set_active (0)
        grid.attach (self.del_naw3, 0, 1, 1, 1)
        grid.attach (self.del_naw3_option, 1, 1, 2, 1)
        
        notebk.append_page (grid, Gtk.Label.new ("حذف"))
        
        #--------------------------------------------------------------------------------------------
        grid = Gtk.Grid ()
        grid.set_margin_bottom (5)
        grid.set_margin_start (5)
        grid.set_margin_top (5)
        grid.set_margin_end (5)
        grid.set_row_spacing (5)
        grid.set_column_spacing (5)
        
        self.replace_text = Gtk.Button.new_with_label ("استبدال نص")
        self.replace_text.connect ("clicked", self.set_option_replace)
        self.entry_replace_text_old = Gtk.Entry ()
        self.entry_replace_text_new = Gtk.Entry ()
        self.re_replace_text = Gtk.CheckButton.new_with_label ("تعبير قياسي (النص الأول)")
        grid.attach (self.replace_text, 0, 0, 1, 1)
        grid.attach (self.entry_replace_text_old, 1, 0, 3, 1)
        grid.attach (self.entry_replace_text_new, 4, 0, 3, 1)
        grid.attach (self.re_replace_text, 7, 0, 1, 1)
        
        self.replace_naw3 = Gtk.Button.new_with_label ("استبدال نوع")
        self.replace_naw3.connect ("clicked", self.set_option_replace)
        self.replace_naw3_option= Gtk.ComboBoxText ()
        for a in ["الحروف العربية", "الحروف اللاتينة الصغيرة", "الحروف اللاتينة الكبيرة",
                   "كل الحروف اللاتينة", "الأرقام", "الرموز", "المسافات",
                   "كل شيء ما عدا الحروف العربية", "كل شيء ما عدا الحروف اللاتينة",
                   "كل شيء ما عدا الأرقام", "كل شيء ما عدا الرموز", "كل شيء"]:
            self.replace_naw3_option.append_text (a)
        self.replace_naw3_option.set_active (0)
        self.entry_replace_naw3_new = Gtk.Entry ()
        grid.attach (self.replace_naw3, 0, 1, 1, 1)
        grid.attach (self.replace_naw3_option, 1, 1, 3, 1)
        grid.attach (self.entry_replace_naw3_new, 4, 1, 3, 1)
        
        notebk.append_page (grid, Gtk.Label.new ("استبدال"))
        
        #----------------------------------------------------------------------------------
        self.store_option = Gtk.ListStore (GObject.TYPE_STRING, GObject.TYPE_STRING)
        hbox1 = Gtk.Box (spacing=3,orientation=Gtk.Orientation.HORIZONTAL)
        btnbox2 = Gtk.Box (orientation=Gtk.Orientation.VERTICAL)
        Gtk.StyleContext.add_class (btnbox2.get_style_context (), "linked")
        
        btnbox2.append (Gtk.Label.new (" "))
        
        go_up2 = Gtk.Button.new_from_icon_name ("go-up-symbolic")
        go_up2.set_tooltip_text ("ارفع العملية المحدد")
        go_up2.connect ("clicked", lambda *a: self.move_file_cb (self.sel_option, -1))
        btnbox2.append (go_up2)
        
        go_down2 = Gtk.Button.new_from_icon_name ("go-down-symbolic")
        go_down2.set_tooltip_text ("أنزل العملية المحدد")
        go_down2.connect ("clicked", lambda *a: self.move_file_cb (self.sel_option, 1))
        btnbox2.append (go_down2)
        
        remove2 = Gtk.Button.new_from_icon_name ("edit-delete-symbolic")
        remove2.set_tooltip_text ("حذف العملية المحددة")
        remove2.connect ("clicked", lambda *a: self.sure ("هل تريد حذف العملية المحددة؟", 
                                            self.sel_option, self.remove_option_cb, 0))
        btnbox2.append (remove2)
        
        remove_all2 = Gtk.Button.new_from_icon_name ("edit-clear-all-symbolic")
        remove_all2.set_tooltip_text ("حذف جميع العمليات")
        remove_all2.connect ("clicked", lambda *a: self.sure ("هل تريد حذف جميع العمليات؟", 
                                            self.sel_option, self.remove_all_options_cb, 4))
        btnbox2.append (remove_all2)
        
        #-------------------------------------------------------------------------------------------
        self.tree_option = Gtk.TreeView.new_with_model (self.store_option)
        self.sel_option = self.tree_option.get_selection ()
        #self.tree_option.set_rules_hint (True)
        celltext = Gtk.CellRendererText ()
        celltext.set_property ("ellipsize", Pango.EllipsizeMode.END)
        columntext= Gtk.TreeViewColumn ("العمليات", celltext, text = 1 )
        self.tree_option.append_column (columntext)
        scroll = Gtk.ScrolledWindow ()
        scroll.set_child (self.tree_option)
        scroll.set_hexpand (True)
        hbox1.append (scroll)
        hbox1.append (btnbox2)
        
        vbox_main.append (hbox1)
        vbox_main.append (notebk)
        
        #-------------------------------------------------------------------------------------------
        self.win.set_child (vbox_main)
        self.win.show ()

        
        

class custom_object (object):
    
    def pos_object_cb (self, btn):
        if btn.get_active () == 1:
            self.pos_after.set_sensitive (True)
        else:
            self.pos_after.set_sensitive (False)
    
    def __init__ (self, name, placeholder, ls, signal0):
        self.check_object = Gtk.CheckButton.new_with_label (name)
        self.check_object.connect ("toggled", signal0)
        self.entry_start = Gtk.Entry ()
        self.entry_start.set_placeholder_text (placeholder)
        self.entry_start.connect ("changed", signal0)
        self.type_object= Gtk.ComboBoxText ()
        for a in ls:
            self.type_object.append_text (a)
        self.type_object.set_active (0)
        self.type_object.connect ("changed", signal0)
        self.pos_object= Gtk.ComboBoxText ()
        for a in ["في الأول", "في الوسط", "في الأخير"]:
            self.pos_object.append_text (a)
        self.pos_object.set_active (0)
        self.pos_object.connect ("changed", signal0)
        self.pos_object.connect ("changed", self.pos_object_cb)
        adj = Gtk.Adjustment.new (1, 1, 100, 1, 5, 0)
        self.pos_after = Gtk.SpinButton ()
        self.pos_after.set_adjustment (adj)
        self.pos_after.set_value (1.0)
        self.pos_after.connect ("changed", signal0)
        self.pos_after.set_sensitive (False)
        self.reverse = Gtk.CheckButton.new_with_label ("عكسي   ")
        self.reverse.connect ("toggled", signal0)
        self.add = Gtk.Button.new_with_label (name)
        
          
    

if __name__ == "__main__":
    app = RenameAll ()
    app.run ()
    
    
